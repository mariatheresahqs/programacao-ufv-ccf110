## Programação-UFV-ccf110

Códigos referentes as listas de exercícios da disciplina de Programação - CCF110 (Universidade Federal de Viçosa-UFV). 

------
##### OBJETIVOS
* Conhecer noções de algoritmos, programas e linguagens de programação de alto nível.
* Compreender o paradigma de programação estruturado, assim como manipular tipos de dados, variáveis e
constantes.
* Implementar em uma linguagem de programação as estruturas de controle e repetição, variáveis compostas
homogêneas (vetores e matrizes) e heterogêneas (registros).
* Conhecer a importância da modularização, documentação e testes de programas.
* Manipular entrada e saída de informações armazenadas em arquivos e compreender o conceito de recursividade.


### CONTEÚDO PROGRAMÁTICO
------

**1. Noções de algoritmos, programas e linguagens de programação de alto nível** 

  1.1. Definição de algoritmos e tipos de representação.  
  1.1.1.Descrição Narrativa, Fluxograma e Pseudocódigo/Português Estruturado/Portugol.   
  1.2. Linguagem de máquina e de montagem. 
  1.3. Linguagem de programação de alto nível. 

**2. Paradigmas de programação**

  2.1. Programação Imperativa (Programação Estruturada).  
  2.2. Outros paradigmas: Orientado a Objetos, Funcional e Lógico. 

**3. Conceitos e Elementos básicos da Programação**
  
  3.1. Tipos de Dados (simples e compostos).   
  3.2. Variáveis e Constantes.   
  3.2.1.Conceitos, declaração e inicialização.   
  3.2.2.Atribuição de valores.   
  3.3. Operadores lógicos, aritméticos e relacionais.  
  3.4. Comandos de entrada e saída.   
  3.5. Comentários no código dos programas.   
  3.6. Testes de Mesa.   

**4. Paradigma de Programação Estruturada e Introdução à Linguagem C**
  
  4.1. Estruturas sequenciais.   
  4.2. Estruturas condicionais (if-then-else / Se-Então-Senão).   
  4.3. Estruturas iterativas (for/Para, while/Enquanto, do-while/Faz-Enquanto).   

**5. Variáveis compostas homogêneas** 
  
  5.1. Variáveis compostas homogêneas unidimensionais - Vetores.   
  5.2. Indexação, inserção, alteração e consulta.   

**6. Variáveis compostas homogêneas multidimensionais - Matrizes**
  
  6.1. Indexação, inserção, alteração e consulta.   

**7. Ponteiros**
  
  7.1. Declaração de ponteiros.   
  7.3. Ponteiros e variáveis indexadas (vetores e matrizes).   

**8. Variáveis compostas heterogêneas (Registros)**
  
  8.1. Declaração, inicialização e atribuição de registros.   
  8.2. Uso de ponteiros em registros.   

**9. Modularização de programas**
  
  9.1. Conceito de modularização.   
  9.2. Parâmetros formais e parâmetros reais.   
  9.3. Modalidades para passagem de parâmetros: valor ou referência.   

**10. Arquivos**
  
  10.1. Arquivo texto e arquivo binário.   
  10.2. Leitura e escrita em arquivos.   
  10.3. Abertura, utilização e fechamento de arquivos.   

**11. Recursividade**
  
  11.1. Conceitos básicos.   
  11.2. Exemplos de recursividade.   

### BIBLIOGRAFIA BÁSICA
------
 
1. D. E. Knuth. The Art of Computer Programming, Volume 1: Fundamental Algorithms, Addison-Wes - ley, 1997.  
2. A. L. V. Forbellone, H. F. Eberspacher, Lógica de Programação: a Construção de Algoritmos e Estruturas de Dados, Makron Books, 2005.
3. N. Ziviani, Projeto de Algoritmos com Implementações em Pascal e C, Editora Thomson, 2004.

### BIBLIOGRAFIA COMPLEMENTAR
------

1. J. A. G. Manzano, Algoritmos: lógica para desenvolvimento de programação de computadores, Editora Érica, 2004.
2. T. H. Cormen; C. E. Leiserson; R.L. Rivest. Algoritmos: teorema e prática. Campus, 2002.
3. H. M. Deitel, P. J. Deitel, C - Como Programar , 6a Ed., Pearson Education - Br, 2011.
4. J. Tremblay, Ciência dos computadores: uma abordagem algorítmica, Makron Books, 1983.
5. H. Farrer, Algoritmos Estruturados. Rio de Janeiro: LTC, 1999.
6. P. Feofiloff. Algoritmos Estruturados. Rio de Janeiro: Elsevier, 2009.